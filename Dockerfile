FROM tensorflow/tensorflow:1.13.1-py3

ADD malaria.py /opt/malaria.py
RUN chmod +x /opt/malaria.py

ENTRYPOINT ["/usr/local/bin/python"]
CMD ["/opt/malaria.py"]
