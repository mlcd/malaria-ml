# Malaria-ML

I created malaria-ml to explore ways of combining the best CI/CD and ML frameworks to continuously deploy TensorFlow models. Malaria-ml is based on a 2018 paper by Rajaraman et al. (<https://peerj.com/articles/4568/>) on using Convolutional Neural Networks to improve accuracy in detecting malaria in thin blood smear images.

## Download Dataset

Along with their paper, Rajaraman et al. published a dataset of 27,558 cell images split 50/50 between parasitized and uninfected cells. The dataset is available from the NIH at <https://ceb.nlm.nih.gov/repositories/malaria-datasets/> as well as from Kaggle at <https://www.kaggle.com/iarunava/cell-images-for-detecting-malaria>.

For now, malaria-ml assumes you've downloaded the dataset and specified the path to the cell_images root in --tf-data-dir.

## Network Architecture

Malaria-ml currently uses 6 layers:

* 32-filter convolutional layer with 3x3 kernel and 2x2 max pooling
* 32-filter convolutional layer with 3x3 kernel and 2x2 max pooling
* Flatten layer
* 128-node fully connected layer
* Dropout layer
* Output layer

## Train and Evaluate the Model

To train and evaluate the model locally, run

```bash
python malaria.py
```

Given default arguments, current model achieves > 95% accuracy in less than 2,000 training steps.