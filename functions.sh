# functions.sh

function current-release()
{
    git rev-list --count --no-merges master
}

function next-release()
{
    echo $(($(current-release) + 1))
}

function version-number()
{
    local branch=$1
    local commitHash=$2
    if [ "$branch" == "" ] || [ "$commitHash" == "" ]; then
        echo "Usage: version-number branch commit-hash"
        return 1
    fi
    
    if [ "$branch" == "master" ]; then
        echo "1.$(current-release)"
    else
        echo "1.$(next-release)-$commitHash"
    fi
}
