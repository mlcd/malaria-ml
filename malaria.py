#
# malaria.py
#
# Trains, evaluates, and exports malaria model
#

import argparse
import numpy as np
from pathlib import Path
import random
import tensorflow as tf
from tensorflow.estimator.inputs import numpy_input_fn
from tensorflow.data.experimental import AUTOTUNE

#
# parse_arguments
#
def parse_arguments():
  parser = argparse.ArgumentParser()
  
  parser.add_argument("--tf-data-dir",
                      type=str,
                      default="/tmp/data/",
                      help="GCS path or local path of training data.")

  parser.add_argument('--tf-model-dir',
                      type=str,
                      help='GCS path or local directory.')
  
  parser.add_argument('--tf-export-dir',
                      type=str,
                      default='export/',
                      help='GCS path or local directory to export model')
  
  parser.add_argument("--tf-reshape-width",
                      type=int,
                      default=150,
                      help="Width of images after reshaping")
  
  parser.add_argument("--tf-reshape-height",
                      type=int,
                      default=150,
                      help="Height of images after reshaping")
  
  parser.add_argument('--tf-batch-size',
                      type=int,
                      default=100,
                      help='The number of batches to use for training')
  
  parser.add_argument('--tf-train-steps',
                      type=int,
                      default=5000,
                      help='The number of training steps to perform.')
  
  return parser.parse_args()

#
# load_and_preprocess_image
#
def load_and_preprocess_image(path, width, height):
  image = tf.image.decode_png(tf.read_file(path))
  image = tf.image.resize(image, (width, height))
  image.set_shape([width, height, 3])
  image = image / 255.0
  return image

#
# load_dataset
#
def load_dataset(data_dir):
  root = Path(data_dir)
  
  # Lookup and shuffle image paths
  paths = list(root.glob('*/*.png'))
  paths = [str(path) for path in paths]    
  random.shuffle(paths)
  
  # Lookup labels based on folder
  label_names = sorted(item.name for item in root.glob('*/') if item.is_dir())
  label_to_index = dict((name, index) for index,name in enumerate(label_names))  
  labels = [label_to_index[Path(path).parent.name] for path in paths]
  
  return np.asarray(paths), np.asarray(labels)

#
# split_dataset
#
def split_dataset(images, labels):  
  split = int(0.7 * len(images))
  train_images = images[0:split]
  train_labels = labels[0:split]
  test_images = images[split:]
  test_labels = labels[split:]
  return train_images, train_labels, test_images, test_labels

#
# model_fn
#
def model_fn(features, labels, mode, params, config):
  "Defines the TensorFlow model"
  
  tf.logging.info("features %s", features)
  tf.logging.info("labels %s", labels)

  # Log input to TensorBoard
  tf.summary.image("inputs", features)

  #
  # Extract Features
  #

  # Layer 1: Convolve and Pool
  input_shape = [params["image_width"], params["image_height"], 3]
  tensor = tf.keras.layers.Conv2D(32, 3, padding="same", activation=tf.nn.relu, input_shape=input_shape, name="conv1")(features)
  tensor = tf.keras.layers.MaxPooling2D((2, 2), padding="same", name="pool1")(tensor)

  tf.logging.info("layer1 %s", tensor)

  # Layer 2: Convolve and Pool
  tensor = tf.keras.layers.Conv2D(32, 3, padding="same", activation=tf.nn.relu, name="conv2")(tensor)
  tensor = tf.keras.layers.MaxPooling2D((2, 2), padding="same", name="pool2")(tensor)

  tf.logging.info("layer2 %s", tensor)

  #
  # Classify Features
  #

  # Layer 3: Flatten
  tensor = tf.keras.layers.Flatten()(tensor)

  tf.logging.info("layer3 %s", tensor)

  # Layer 4: Fully connected 
  tensor = tf.keras.layers.Dense(128, activation=tf.nn.relu, name="fc")(tensor)

  tf.logging.info("layer4 %s", tensor)

  # Layer 5: Dropout
  tensor = tf.keras.layers.Dropout(0.4, name="dropout")(tensor)

  tf.logging.info("layer5 %s", tensor)

  # Layer 6: Classifier
  logits = tf.keras.layers.Dense(2, name="classifier")(tensor)    
  classes = tf.argmax(logits, axis=1)

  tf.logging.info("layer6 %s", logits)

  #
  # Predict Mode
  #

  if mode == tf.estimator.ModeKeys.PREDICT:        
      predictions = {
          'classes': classes,
          'probabilities': tf.nn.softmax(logits),
      }
      return tf.estimator.EstimatorSpec(mode, predictions=predictions)

  loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)
  accuracy = tf.metrics.accuracy(labels=labels, predictions=classes)

  #
  # Eval Mode
  #

  if mode == tf.estimator.ModeKeys.EVAL:
    return tf.estimator.EstimatorSpec(mode, loss=loss, eval_metric_ops={ "accuracy": accuracy })

  #
  # Train Mode
  #

  assert mode == tf.estimator.ModeKeys.TRAIN

  # Log training accuracy to TensorBoard
  tf.summary.scalar("train_accuracy", accuracy[1])

  optimizer = tf.train.AdamOptimizer()
  train_op = optimizer.minimize(loss, tf.train.get_or_create_global_step())

  return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

#
# main
#
def main(_):
  tf.logging.set_verbosity(tf.logging.INFO)
  
  args = parse_arguments()
  
  # Load dataset
  images, labels = load_dataset(args.tf_data_dir)
  
  tf.logging.info("Loaded %d images", len(images))
  
  train_images, train_labels, test_images, test_labels = split_dataset(images, labels)

  tf.logging.info("Split %d images into %d training and %d testing", len(images), len(train_images), len(test_images))
  
  # Create estimator
  config = tf.estimator.RunConfig(model_dir = args.tf_model_dir,
                                  save_summary_steps = 20,
                                  keep_checkpoint_max = 1)
  
  params = {
    "image_width": args.tf_reshape_width,
    "image_height": args.tf_reshape_height
  }
  
  estimator = tf.estimator.Estimator(model_fn, config = config, params = params)
  
  #
  # train_input_fn
  #
  def train_input_fn():
    "Supplies training data in never-ending sequence of minibatches"
    
    ds = tf.data.Dataset.from_tensor_slices((train_images, train_labels))
    ds = ds.map(lambda path, label:(load_and_preprocess_image(path, params["image_width"], params["image_height"]), label), num_parallel_calls=AUTOTUNE)
    return ds.batch(args.tf_batch_size).repeat()

  train_spec = tf.estimator.TrainSpec(input_fn = train_input_fn, 
                                      max_steps = args.tf_train_steps)
  
  #
  # eval_input_fn
  #
  def eval_input_fn():
    "Supplies eval data in single batch"
    
    ds = tf.data.Dataset.from_tensor_slices((test_images, test_labels))
    ds = ds.map(lambda path, label:(load_and_preprocess_image(path, params["image_width"], params["image_height"]), label), num_parallel_calls=AUTOTUNE)
    return ds.batch(len(test_images))

  eval_spec = tf.estimator.EvalSpec(input_fn = eval_input_fn,
                                    steps = 1,
                                    start_delay_secs = 1,
                                    throttle_secs = 1)
  
  # Train
  tf.logging.info("Training and evaluating")  
  tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)  
  tf.logging.info("Training done")
  
if __name__ == '__main__':
  tf.app.run()
